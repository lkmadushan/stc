## STC Application

> STC frontend web application.

#### 1. How to run

###### 1.1 Make sure you have already installed [Node](https://www.nodejs.org).

###### 1.2 Clone repository
```bash
git clone {repository-path} app
```
###### 1.3 Start application
```bash
cd app
npm install
npm run serve
```

###### 1.3 You can access application from [localhost:8080](http://localhost:8080)

#### Deployment
###### Deploy Local

```bash
npm run build
```

###### Deploy Development

```bash
npm run build:dev
```

###### Deploy Production

```bash
npm run build:prod
```

> You can upload dist folder to any server

#### 3. Rebuild application
> Make sure you have already installed [Node](https://nodejs.org).
```bash
# install dependancies
npm install
```

```
# serve for development
npm run serve
```

```
# build for production
npm run build
```

# Download language file
docker exec $(docker-compose ps -q ruby) bash -c 'wti pull'
