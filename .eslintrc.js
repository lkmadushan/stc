module.exports = {
    root: true,
    env: {
        node: true,
        jquery: true,
        browser: true,
    },
    globals: {
        _: true,
        Vue: true,
        Events: true,
    },
    extends: ['plugin:vue/base', '@vue/prettier'],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'vue/html-indent': ['error', 4],
    },
    parserOptions: {
        parser: 'babel-eslint',
    },
}
