import { mapGetters } from 'vuex'

export default {
    data() {
        return {
            containerBlcData: {
                ContainerID: '',
                UnloadingAreaID: '',
                CheckpointID: '',
            },
        }
    },

    created() {
        console.log('[Mixins: interact-with-blc] Created')
        this.fetchContainerBlcData()
    },

    watch: {
        selectedContainer(container) {
            console.log('[Mixins: interact-with-blc] selectedContainer')
            this.fetchContainerBlcData(container)
        },
    },

    computed: {
        ...mapGetters(['selectedContainer']),
    },

    methods: {
        async fetchContainerBlcData(container = null) {
            console.log('[Mixins: interact-with-blc] fetchContainerBlcData()')
            container = container || this.selectedContainer

            if (container) {
                let { data } = await this.$http.get(
                    `Container/${container.ContainerID}/BLC`
                )

                this.containerBlcData = data
            }
        },
    },
}
