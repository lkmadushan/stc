export default {
    methods: {
        async fetchActorByID(id) {
            let { data } = await this.$http.get(`Actor/${id}`)

            return data
        },
    },
}
