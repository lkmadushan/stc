export default {
    methods: {
        sortBy(a, b, field) {
            return a[field].toLowerCase().localeCompare(b[field].toLowerCase())
        },
        sortByDate(a, b, field) {
            return new Date(b[field]) - new Date(a[field])
        },
    },
}
