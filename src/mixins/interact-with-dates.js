import { DateTime } from 'luxon'

export default {
    computed: {
        withinDay() {
            let dateTimeFrom = DateTime.local()
                .startOf('day')
                .toJSDate()
            let dateTimeTo = DateTime.local()
                .endOf('day')
                .toJSDate()

            return [dateTimeFrom, dateTimeTo]
        },

        withinFiveDays() {
            let dateTimeFrom = DateTime.local()
                .startOf('day')
                .toJSDate()
            let dateTimeTo = DateTime.local()
                .plus({ days: 5 })
                .endOf('day')
                .toJSDate()

            return [dateTimeFrom, dateTimeTo]
        },

        withinSevenDays() {
            let dateTimeFrom = DateTime.local()
                .startOf('day')
                .toJSDate()
            let dateTimeTo = DateTime.local()
                .plus({ days: 7 })
                .endOf('day')
                .toJSDate()

            return [dateTimeFrom, dateTimeTo]
        },

        thisWeek() {
            let dateTimeFrom = DateTime.local()
                .startOf('week')
                .toJSDate()
            let dateTimeTo = DateTime.local()
                .endOf('week')
                .toJSDate()

            return [dateTimeFrom, dateTimeTo]
        },

        lastWeek() {
            let dateTimeFrom = DateTime.local()
                .minus({ weeks: 1 })
                .startOf('week')
                .toJSDate()
            let dateTimeTo = DateTime.local()
                .minus({ weeks: 1 })
                .endOf('week')
                .toJSDate()

            return [dateTimeFrom, dateTimeTo]
        },

        thisMonth() {
            let dateTimeFrom = DateTime.local()
                .startOf('month')
                .toJSDate()
            let dateTimeTo = DateTime.local()
                .endOf('month')
                .toJSDate()

            return [dateTimeFrom, dateTimeTo]
        },

        lastMonth() {
            let dateTimeFrom = DateTime.local()
                .minus({ months: 1 })
                .startOf('month')
                .toJSDate()
            let dateTimeTo = DateTime.local()
                .minus({ months: 1 })
                .endOf('month')
                .toJSDate()

            return [dateTimeFrom, dateTimeTo]
        },

        currentWeekRange() {
            let dateTimeFrom = DateTime.local()
                .startOf('week')
                .toJSDate()
            let dateTimeTo = DateTime.local()
                .endOf('week')
                .toJSDate()

            return [dateTimeFrom, dateTimeTo]
        },

        currentWeek() {
            return DateTime.local().weekNumber
        },
    },
}
