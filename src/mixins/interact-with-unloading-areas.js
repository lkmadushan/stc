import { DateTime } from 'luxon'
import { mapGetters } from 'vuex'

export default {
    data() {
        return {
            unloadingAreas: [],
        }
    },

    created() {
        this.fetchUnloadingAreas()
    },

    watch: {
        selectedContainer(container) {
            console.log('Interact-with-unloading watch container')
            this.fetchUnloadingAreas(container)
        },
    },

    computed: {
        ...mapGetters(['selectedContainer']),

        activeUnloadingAreas() {
            return this.unloadingAreas.filter(
                unloadingArea => unloadingArea.IsActive
            )
        },
    },

    methods: {
        async fetchUnloadingAreas(container = null) {
            console.log('Interact-with-unloading fetchUnloadingAreas')
            container = container || this.selectedContainer

            if (container) {
                let { data } = await this.$http.get(
                    `Container/${container.ContainerID}/UnloadingArea`
                )

                this.unloadingAreas = data
            }
        },

        async isScheduleOverlappedWithUnloadingArea(
            unloadingArea,
            range,
            actor = null
        ) {
            console.log(
                'Interact-with-unloading isScheduleOverlappedWithUnloadingArea'
            )
            try {
                let dateTimeFormat = process.env.VUE_APP_DATETIME_FORMAT
                let dateTimeFrom = DateTime.fromISO(
                    range[0].toISOString()
                ).toFormat(dateTimeFormat)
                let dateTimeTo = DateTime.fromISO(
                    range[1].toISOString()
                ).toFormat(dateTimeFormat)

                let endpoint = `UnloadingArea/${
                    unloadingArea.UnloadingAreaID
                }/OccupancyValidation/From/${dateTimeFrom}/To/${dateTimeTo}`

                if (actor)
                    endpoint = `${endpoint}?onBehalfOfActorId=${actor.ActorID}`

                await this.$http.get(endpoint)
            } catch (e) {
                if (e.response.status == 409) {
                    return true
                }
            }

            return false
        },
    },
}
