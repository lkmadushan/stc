import _ from 'lodash'
import { DateTime } from 'luxon'
import { mapGetters } from 'vuex'

let InteractWithSchedules = {
    data() {
        let dateTimeFormat = process.env.VUE_APP_DATETIME_FORMAT

        return {
            schedules: [],
            dateTimeFormat,
        }
    },

    created() {
        this.fetchSchedules()
    },

    watch: {
        selectedContainer: 'fetchSchedules',
    },

    computed: {
        ...mapGetters(['selectedContainer']),
    },

    methods: {
        async fetchSchedules() {
            let { data } = await this.$http.get(
                `/Container/${
                    this.selectedContainer.ContainerID
                }/ScheduleTemplate`
            )

            this.schedules = data
        },

        buildAccessSetWithoutGates(data) {
            let accessSet = []
            const gate = {
                CheckpointID: null,
            }

            accessSet.push(this.buildGateAccessForDirection(gate, data, 'in'))

            return accessSet
        },

        buildAccessSet(data, actorType = null) {
            let accessSet = []

            if (data.hasOwnProperty('allowGates') && data.allowGates) {
                const gates = _.chain(data.scheduledGates)
                    .flatMap(val => val)
                    .uniq()
                    .value()

                gates.forEach(gate => {
                    accessSet.push(this.buildGateAccessForDirection(gate, data))
                })
            } else {
                data.gates.forEach(gate => {
                    if (gate.DirectionInEnabled) {
                        accessSet.push(
                            this.buildGateAccessForDirection(
                                gate,
                                data,
                                'in',
                                actorType
                            )
                        )
                    }

                    if (gate.DirectionOutEnabled) {
                        accessSet.push(
                            this.buildGateAccessForDirection(
                                gate,
                                data,
                                'out',
                                actorType
                            )
                        )
                    }
                })
            }

            return accessSet
        },

        buildGateAccessForDirection(
            gate,
            data,
            direction = null,
            actorType = null
        ) {
            let access = {}

            access['Schedules'] = []
            access['CheckPointID'] = gate['CheckpointID']

            if (actorType === 'MILKRUN') {
                access['DestinationContainerID'] =
                    data.unloadingArea.ContainerID
            } else {
                access[
                    'DestinationContainerID'
                ] = this.selectedContainer.ContainerID
            }

            if (data.hasOwnProperty('unloadingArea') && data.unloadingArea) {
                access['UnloadingAreaID'] =
                    data.unloadingArea.UnloadingAreaID || data.unloadingArea
            }

            if (data.hasOwnProperty('recipientName')) {
                access['RecipientName'] = data.recipientName
            }

            if (data.hasOwnProperty('recipientCompany')) {
                access['RecipientCompany'] = data.recipientCompany
            }

            if (data.hasOwnProperty('recipientPhone')) {
                access['RecipientPhone'] = data.recipientPhone
            }

            if (data.hasOwnProperty('requestedResources')) {
                access['RequestedResources'] = data.requestedResources
            }

            if (direction) {
                let part = _.cloneDeep(_.first(data.schedule['ScheduleParts']))
                delete part['SchedulePartTemplateID']
                part['Direction'] = direction.toUpperCase()

                if (
                    data.hasOwnProperty('dateTimeRange') &&
                    data.dateTimeRange.length
                ) {
                    part['ValidFrom'] = DateTime.fromISO(
                        data.dateTimeRange[0].toISOString()
                    ).toFormat(this.dateTimeFormat)
                    part['ValidTo'] = DateTime.fromISO(
                        data.dateTimeRange[1].toISOString()
                    ).toFormat(this.dateTimeFormat)
                }

                if (data.hasOwnProperty('preFlex')) {
                    part['PreFlex'] = data.preFlex
                }

                access['Schedules'] = [
                    {
                        ScheduleParts: [part],
                        Description: data.schedule.Description,
                    },
                ]
            }

            if (data.hasOwnProperty('scheduledGates')) {
                access['ScheduleTemplates'] = []

                Object.keys(data.scheduledGates).forEach(schedule => {
                    if (
                        _.findIndex(data.scheduledGates[schedule], {
                            CheckpointID: gate['CheckpointID'],
                        }) > -1
                    ) {
                        access['ScheduleTemplates'].push(schedule)
                    }
                })
            }

            return access
        },
    },
}

export default InteractWithSchedules
