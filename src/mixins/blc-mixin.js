import { mapGetters } from 'vuex'

export default {
    data() {
        return {
            containerBlcData: {
                ContainerID: '',
                UnloadingAreaID: '',
                CheckpointID: '',
            },
        }
    },

    created() {
        console.log('[BlcMixin] created')
    },

    watch: {
        // selectedContainer(container) {
        //     console.log('[Mixins: interact-with-blc] selectedContainer')
        //     this.fetchContainerBlcData(container)
        // },
    },

    computed: {
        ...mapGetters(['selectedContainer']),
    },

    methods: {
        async getContainerBlcData(container = null) {
            console.log('[BlcMixin] getContainerBlcData()')
            container = container || this.selectedContainer

            if (container) {
                let { data } = await this.$http.get(
                    `Container/${container.ContainerID}/BLC`
                )

                this.containerBlcData = data
            }
        },
    },
}
