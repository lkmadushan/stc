import _ from 'lodash'
import { mapGetters } from 'vuex'

let InteractWithGates = {
    data() {
        return {
            gatesResponse: [],
        }
    },

    created() {
        console.log('[GatesMixin] created')
    },

    watch: {
        // selectedContainer(container) {
        //     this.fetchGates(container)
        // },
    },

    computed: {
        uniqueGates() {
            return _.uniqBy(this.gates, 'Name')
        },

        gates() {
            let gates = this.gatesResponse.filter(gate => {
                return gate.DirectionInEnabled || gate.DirectionOutEnabled
            })

            return _.sortBy(gates, 'Name')
        },

        inGates() {
            let gates = _.cloneDeep(this.gates)

            return gates
                .filter(gate => {
                    return gate.DirectionInEnabled
                })
                .map(gate => {
                    gate.DirectionOutEnabled = false

                    return gate
                })
        },

        outGates() {
            let gates = _.cloneDeep(this.gates)

            return gates
                .filter(gate => {
                    return gate.DirectionOutEnabled
                })
                .map(gate => {
                    gate.DirectionInEnabled = false

                    return gate
                })
        },

        ...mapGetters(['selectedContainer', 'permissions']),
    },

    methods: {
        async getGates(container = null) {
            console.log('[GatesMixin] getGates')
            container = container || this.selectedContainer

            if (this.permissions.includes('CHECKPOINT_READ') && container) {
                let { data } = await this.$http.get(
                    `Container/${container.ContainerID}/Checkpoint`
                )

                this.gatesResponse = data
            }
        },
    },
}

export default InteractWithGates
