import { mapGetters } from 'vuex'

export default {
    data() {
        return {
            recipientData: [],
        }
    },

    created() {
        console.log('[RecipientsMixin] created')
    },

    watch: {
        selectedContainer(container) {
            this.fetchRecipients(container)
        },
    },

    computed: {
        ...mapGetters(['selectedContainer']),

        recipients() {
            return this.recipientData.map(item => {
                let company = item.Company ? ` (${item.Company})` : ''

                item['Label'] = `${item.FirstName} ${item.LastName}${company}`

                return item
            })
        },
    },

    methods: {
        async getRecipients(container = null) {
            console.log('[RecipientsMixin] getRecipients')
            container = container || this.selectedContainer

            if (container) {
                let { data } = await this.$http.get(
                    `Container/${container.ContainerID}/User`
                )

                this.recipientData = data
            }
        },
    },
}
