import Vue from 'vue'
import i18next from 'i18next'
import XHR from 'i18next-xhr-backend'
import VueI18Next from '@panter/vue-i18next'

Vue.use(VueI18Next)

const options = {
    lng: 'null',
    backend: {
        loadPath: '/languages/{{ lng }}.json',
    },
}

if (process.env.VUE_APP_LANG_FALLBACK == 'true') {
    options['fallbackLng'] = 'en'
}

i18next.use(XHR).init(options)

export default new VueI18Next(i18next)
