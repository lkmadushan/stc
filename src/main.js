import 'babel-polyfill'
import './auth'
import Vue from 'vue'
import i18n from './i18n'
import store from './store'
import jQuery from 'jquery'
import moment from 'moment'
import router from './router'
import Vuetify from 'vuetify'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'fullcalendar/dist/locale/sv.js'
import VueLazyLoad from 'vue-lazyload'
import VueFuse from 'vue-fuse'
import VueSwatches from 'vue-swatches'
import VueAnalytics from 'vue-analytics'
import VueCookies from 'vue-cookies'

import App from './views/App'

window.jQuery = window.$ = jQuery

window.Vue = Vue
window.Events = new Vue()

Vue.use(ElementUI, {
    locale,
})

Vue.use(VueCookies)
Vue.use(VueFuse)
Vue.use(Vuetify)
Vue.use(VueLazyLoad)
Vue.use(VueSwatches)

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('YYYY-MM-DD')
    }
})

Vue.filter('formatDateTime', function(value) {
    if (value) {
        return moment(String(value)).format('YYYY-MM-DD HH:mm:ss')
    }
})

console.log(
    'App running on %s mode: %s',
    process.env.VUE_APP_ENVIRONMENT,
    process.env.VUE_APP_API_URL
)
Vue.axios.defaults.baseURL = process.env.VUE_APP_API_URL
const isProd = process.env.VUE_APP_ENVIRONMENT === 'production'

// Google Analytics Integration
Vue.use(VueAnalytics, {
    id: 'UA-134636162-1',
    router,
    debug: {
        enabled: false,
        sendHitTask: isProd,
    },
    autoTracking: {
        screenview: true,
        pageviewOnLoad: false,
        pageviewTemplate(route) {
            return {
                page: route.path,
                title: document.title,
                location: window.location.href,
            }
        },
    },
})

// App Initialization
new Vue({
    el: '#app',
    i18n,
    store,
    router,
    render: h => h(App),
})
