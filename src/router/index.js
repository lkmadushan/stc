import Vue from 'vue'
import Router from 'vue-router'
// import Supplier from "../views/supplier/Supplier";

const LoginView = () => import('./../views/Login')
const ForgotPasswordView = () => import('./../views/ForgotPassword')
const ResetPasswordView = () => import('./../views/ResetPassword')
const LayoutView = () => import('../views/Layout')
const UsersView = () => import('../views/users/Users')
const SupplierView = () => import('../views/supplier/Supplier')
const Containers = () => import('../views/Containers')
const DashboardView = () => import('./../views/Dashboard')
const BookingsView = () => import('../views/bookings/Bookings')
const CheckpointsView = () => import('../views/checkpoints/Checkpoints')
const RoutePlannerView = () => import('../views/blc/route-planner/RoutePlanner')
const UnloadingAreasView = () =>
    import('../views/unloading-areas/UnloadingAreas')
const GlobalScheduleView = () =>
    import('../views/global-schedule/GlobalSchedule')
const MaterialManagerView = () =>
    import('../views/blc/material-manager/MaterialManager')
const FollowUpView = () => import('../views/follow-up/FollowUp')
const EventToolView = () => import('../views/event-tool/EventTool')
const VehicleToolView = () => import('../views/vehicle-tool/VehicleTool')

Vue.use(Router)

const routes = [
    {
        path: '/login',
        name: 'login',
        component: LoginView,
        meta: {
            auth: false,
            title: 'Login',
        },
    },
    {
        path: '/forgot-password',
        name: 'forgot-password',
        component: ForgotPasswordView,
        meta: {
            auth: false,
            title: 'Forgot Password',
        },
    },
    {
        path: '/reset-password/:token',
        name: 'reset-password',
        component: ResetPasswordView,
        meta: {
            auth: false,
            title: 'Reset Password',
        },
    },
    {
        path: '/',
        component: LayoutView,
        meta: {
            auth: true,
            title: 'Dashboard',
        },
        children: [
            {
                path: '/',
                name: 'dashboard',
                component: DashboardView,
                meta: {
                    title: 'Dashboard',
                },
            },
            {
                path: '/bookings',
                name: 'bookings',
                component: BookingsView,
                meta: {
                    title: 'Bookings',
                },
            },
            {
                path: '/users',
                name: 'users',
                component: UsersView,
                meta: {
                    title: 'Users',
                },
            },
            {
                path: '/supplier',
                name: 'supplier',
                component: SupplierView,
                meta: {
                    title: 'Supplier',
                },
            },
            {
                path: '/vehicleTool',
                name: 'vehicleTool',
                component: VehicleToolView,
                meta: {
                    title: 'Vehicles',
                },
            },
            {
                path: '/containers',
                name: 'containers',
                component: Containers,
                meta: {
                    title: 'Containers',
                },
            },
            {
                path: '/checkpoints',
                name: 'checkpoints',
                component: CheckpointsView,
                meta: {
                    title: 'Checkpoints',
                },
            },
            {
                path: '/unloading-areas',
                name: 'unloading-areas',
                component: UnloadingAreasView,
                meta: {
                    title: 'Unloading areas',
                },
            },
            {
                path: '/global-schedule',
                name: 'global-schedule',
                component: GlobalScheduleView,
                meta: {
                    title: 'Global schedule',
                },
            },
            {
                path: '/blc/material-manager',
                name: 'blc/material-manager',
                component: MaterialManagerView,
                meta: {
                    title: 'Material manager',
                },
            },
            {
                path: '/blc/route-planner',
                name: 'blc/route-planner',
                component: RoutePlannerView,
                meta: {
                    title: 'Route planner',
                },
            },
            {
                path: '/blc/follow-up',
                name: 'blc/follow-up',
                component: FollowUpView,
                meta: {
                    title: 'Follow up',
                },
            },
            {
                path: '/event-tool',
                name: 'eventTool',
                component: EventToolView,
                meta: {
                    title: 'Event Tool',
                },
            },
        ],
    },
]

let router = new Router({
    linkExactActiveClass: 'active',
    base: __dirname,
    mode: 'history',
    fallback: false,
    hashbang: false,
    routes,
})

router.beforeEach((to, from, next) => {
    document.title = to.meta.title + ' | STC'
    next()
})

export default router
