import Vue from 'vue'
import Vuex from 'vuex'
import i18next from 'i18next'
import container from './container'
import { SET_LANGUAGE } from './mutations'
import locale from 'element-ui/lib/locale'
import enLocale from 'element-ui/lib/locale/lang/en'
import svLocale from 'element-ui/lib/locale/lang/sv-SE'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        language: '',
        bookingDetails: [
            {
                id: 1,
                name: 'reference',
                text: 'form.label.reference',
                value: 'bookingDetails.reference',
                checked: true,
                isHighlighted: true,
            },
            {
                id: 2,
                name: 'supplier',
                text: 'data.label.supplier',
                value: 'bookingDetails.supplier',
                checked: true,
                isHighlighted: true,
            },
            {
                id: 3,
                name: 'bookedBy',
                text: 'form.label.bookedBy',
                value: 'bookingDetails.bookedBy',
                checked: true,
                isHighlighted: true,
            },
            {
                id: 4,
                name: 'pin',
                text: 'form.label.pin',
                value: 'bookingDetails.pin',
                checked: true,
                isHighlighted: true,
            },
            {
                id: 5,
                name: 'additionalInfo',
                text: 'form.label.additionalInfo',
                value: 'bookingDetails.additionalInfo',
                checked: true,
                isHighlighted: true,
            },
        ],
    },

    modules: {
        container,
    },

    plugins: [createPersistedState()],

    getters: {
        selectedLanguage(state) {
            let me = Vue.auth.user()
            let lookup = {
                'sv-SE': {
                    name: 'Svenska',
                    flag: 'flag-icon-se',
                    isoName: 'sv-SE',
                },
                'en-US': {
                    name: 'English',
                    flag: 'flag-icon-gb',
                    isoName: 'en-US',
                },
                'no-lang': {
                    name: 'No Lang',
                    flag: '',
                    isoName: 'no-lang',
                },
            }

            return lookup[state.language || me.Culture]
        },
    },

    mutations: {
        [SET_LANGUAGE](state, language) {
            state.language = language
        },
        setBookingDetails(state, bookingDetails) {
            state.bookingDetails = bookingDetails
        },
    },

    actions: {
        changeLanguage({ commit }, language) {
            i18next.changeLanguage(language)

            locale.use(language === 'en-US' ? enLocale : svLocale)

            commit(SET_LANGUAGE, language)
        },
    },
})
