import _ from 'lodash'
import {
    SET_ALL_CONTAINERS,
    SET_BOOKING_COLLAPSED,
    SET_SELECTED_CONTAINER,
    SET_CONTAINER_PERMISSIONS,
} from './mutations'

export default {
    state: {
        containers: [],
        container: '',
        permissions: [],
        collapseBooking: false,
    },

    getters: {
        containerTree(state) {
            let idAttr = 'ContainerID'
            let parentAttr = 'ParentContainerID'
            let childrenAttr = 'Children'

            let tree = []
            let lookup = {}

            let containers = _.cloneDeep(state.containers)

            containers.forEach(obj => {
                lookup[obj[idAttr]] = obj
                obj[childrenAttr] = []
            })

            containers.forEach(obj => {
                if (obj[parentAttr] != null && lookup[obj[parentAttr]]) {
                    lookup[obj[parentAttr]][childrenAttr].push(obj)
                } else {
                    tree.push(obj)
                }
            })

            return tree
        },
        containers(state) {
            return state.containers
        },
        isBlc(state, getters) {
            let container = getters.selectedContainer

            if (_.isEmpty(container)) {
                return false
            }

            return container.ContainerType === 'CCC'
        },

        selectedContainer(state, getters) {
            if (_.isEmpty(state.container)) {
                return _.first(getters.containerTree)
            }

            return state.container
        },

        permissions(state) {
            return state.permissions
        },

        collapseBooking(state) {
            return state.collapseBooking
        },
    },

    mutations: {
        [SET_ALL_CONTAINERS](state, containers) {
            state.containers = containers
        },

        [SET_SELECTED_CONTAINER](state, container) {
            state.container = container
        },

        [SET_CONTAINER_PERMISSIONS](state, permissions) {
            state.permissions = permissions
        },

        [SET_BOOKING_COLLAPSED](state, collapseBooking) {
            state.collapseBooking = collapseBooking
        },
    },

    actions: {
        async fetchAllContainers({ dispatch }) {
            let { data } = await Vue.axios.get('Container')

            dispatch('setAllContainers', data)
        },

        async fetchContainerPermissions(
            { dispatch, getters },
            container = null
        ) {
            container = container || getters.selectedContainer

            if (container) {
                let { data } = await Vue.axios.get(
                    `Container/${container.ContainerID}/Permissions`
                )

                dispatch('setContainerPermissions', data)
            }
        },

        setSelectedContainer({ commit }, container) {
            commit(SET_SELECTED_CONTAINER, container)
        },

        setAllContainers({ commit }, containers) {
            commit(SET_ALL_CONTAINERS, containers)
        },

        setContainerPermissions({ commit }, permissions) {
            commit(SET_CONTAINER_PERMISSIONS, permissions)
        },

        setBookingCollapsed({ commit }, collapseBooking) {
            commit(SET_BOOKING_COLLAPSED, collapseBooking)
        },
    },
}
