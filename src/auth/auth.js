export default {
    request(req, token) {
        this.options.http._setHeaders.call(this, req, {
            Authorization: 'Bearer ' + token,
        })
    },

    response(res) {
        let token = res.data.access_token
        let refresh_token = res.data.refresh_token
        if (refresh_token) {
            window.$cookies.set('refresh_token', refresh_token.trim(), '24d') // 24 day after, expire
        }
        if (token) {
            return token.trim()
        }
    },
}
