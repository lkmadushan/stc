import Vue from 'vue'
import axios from 'axios'
import auth from './auth'
import router from './../router'
import VueAxios from 'vue-axios'
import VueAuth from '@websanova/vue-auth'
import VueAuthAxios from '@websanova/vue-auth/drivers/http/axios.1.x.js'
import VueAuthRouter from '@websanova/vue-auth/drivers/router/vue-router.2.x.js'

Vue.router = router

Vue.use(VueAxios, axios)
Vue.use(VueAuth, {
    auth: auth,
    loginData: {
        url: 'token',
    },
    fetchData: {
        url: 'User/Me',
    },
    refreshData: {
        url: 'token',
        method: 'POST',
        enabled: false,
        interval: 30,
        data: {
            grant_type: 'refresh_token',
            refresh_token: getCookie('refresh_token'),
            client_id: '50B9A4FA-2C5D-41F8-828A-A1F7E29635A2',
        },
    },
    parseUserData(data) {
        return data
    },
    http: VueAuthAxios,
    router: VueAuthRouter,
})

function getCookie(name) {
    const value = '; ' + document.cookie
    const parts = value.split('; ' + name + '=')
    if (parts.length === 2)
        return parts
            .pop()
            .split(';')
            .shift()
}
